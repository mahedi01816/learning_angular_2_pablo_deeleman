class Car {
    private distanceRun: number = 0;
    color: string;

    constructor(public isHybrid: boolean, color: string = 'red') {
        this.color = color;
    }

    getGasConsumption(): string {
        return this.isHybrid ? 'Very low' : 'Too high!';
    }

    drive(distance: number): void {
        this.distanceRun += distance;
    }

    static honk(): string {
        return 'HOOONK!';
    }

    get distance(): number {
        return this.distanceRun;
    }
}