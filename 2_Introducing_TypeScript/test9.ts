export class Greeting {
    constructor(public name: string) {
        console.log(`Hello ${name}`);
    }
}
export class XmasGreeting {
    constructor(public name: string) {
        console.log(`Merry Xmas ${name}`);
    }
}

import greetings = require('Greetings');
var XmasGreetings = greetings.XmasGreetings();
var xmasGreetings = new XmasGreetings('Pete');
// console outputs 'Merry Xmas Pete'