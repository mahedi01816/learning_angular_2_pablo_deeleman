"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Greeting = (function () {
    function Greeting(name) {
        this.name = name;
        console.log("Hello " + name);
    }
    return Greeting;
}());
exports.Greeting = Greeting;
var XmasGreeting = (function () {
    function XmasGreeting(name) {
        this.name = name;
        console.log("Merry Xmas " + name);
    }
    return XmasGreeting;
}());
exports.XmasGreeting = XmasGreeting;
var greetings = require("Greetings");
var XmasGreetings = greetings.XmasGreetings();
var xmasGreetings = new XmasGreetings('Pete');
// console outputs 'Merry Xmas Pete' 
