var Greetings;
(function (Greetings) {
    var Greeting = (function () {
        function Greeting(name) {
            this.name = name;
            console.log("Hello " + name);
        }
        return Greeting;
    }());
    Greetings.Greeting = Greeting;
    var XmasGreeting = (function () {
        function XmasGreeting(name) {
            this.name = name;
            console.log("Merry Xmas " + name);
        }
        return XmasGreeting;
    }());
    Greetings.XmasGreeting = XmasGreeting;
})(Greetings || (Greetings = {}));
var XmasGreeting = Greetings.XmasGreeting;
var xmasGreeting = new XmasGreeting('Joe');
// console outputs 'Merry Xmas Joe' 
