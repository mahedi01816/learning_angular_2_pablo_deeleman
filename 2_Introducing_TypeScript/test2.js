var Car = (function () {
    function Car(isHybrid, color) {
        if (color === void 0) { color = 'red'; }
        this.isHybrid = isHybrid;
        this.distanceRun = 0;
        this.color = color;
    }
    Car.prototype.getGasConsumption = function () {
        return this.isHybrid ? 'Very low' : 'Too high!';
    };
    Car.prototype.drive = function (distance) {
        this.distanceRun += distance;
    };
    Car.honk = function () {
        return 'HOOONK!';
    };
    Object.defineProperty(Car.prototype, "distance", {
        get: function () {
            return this.distanceRun;
        },
        enumerable: true,
        configurable: true
    });
    return Car;
}());
