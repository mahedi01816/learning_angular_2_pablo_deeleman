function delayedGreeting(name): void {
    this.name = name;
    this.greet = function () {
        setTimeout(function () {
            console.log('Hello ' + this.name);
        }, 0);
    };
}

var greeting = new delayedGreeting('Peter')
greeting.greet(); // alerts 'Hello undefined'