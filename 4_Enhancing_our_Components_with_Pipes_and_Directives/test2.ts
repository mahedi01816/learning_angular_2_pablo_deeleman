import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
    name: 'myPipeName',
    pure: false // optional, default is true
})
class MyPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
// We apply transformations to the input value here
        return something;
    }
}
@Component({
        selector: 'my-selector',
        pipes: [MyPipe],
        template: '<p>{{ myVariable | myPipeName: "bar" }}</p>'
    })
class myComponent {
    myVariable: string = 'Foo';
}

@Pipe({
    name: 'pomodoroFormattedTime'
})
class FormattedTimePipe implements PipeTransform {
    transform(totalMinutes: number): string {
        let minutes: number = totalMinutes % 60;
        let hours: number = Math.floor(totalMinutes / 60);
        return `${hours}h:${minutes}m`;
    }
}