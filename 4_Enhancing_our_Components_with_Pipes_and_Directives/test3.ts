import { Directive } from '@angular/core';

@Directive({
    selector: '[selector]',
    inputs: ['inputPropertyName'],
    outputs: ['outputPropertyName'],
    host: {
        '(event1)': 'onMethod1($event)',
        '(target:event2)': 'onMethod2($event)',
        '[prop]': 'expression',
        'attributeName': 'attributeValue'
    },
    providers: [MyCustomType]
})
class myDirective {
    @Input() otherInputPropertyName: any;
    @Output() otherOutputPropertyName: any;
    constructor(myCustomType: MyCustomType) {
// implementation...
    }
}

@Directive({
    selector: '[task]'
})
class TaskTooltipDirective {
    private defaultTooltipText: string;
    @Input() task: Task;
    @Input() taskTooltip: any;
    @HostListener('mouseover')
    onMouseOver() {
        if(!this.defaultTooltipText && this.taskTooltip) {
            this.defaultTooltipText = this.taskTooltip.innerText;
        }
        this.taskTooltip.innerText = this.task.name;
    }
    @HostListener('mouseout')
onMouseOut() {
    if(this.taskTooltip) {
        this.taskTooltip.innerText = this.defaultTooltipText;
    }
}
}