@Component({
    selector: "greeting",
    template: "HELLO {{ name | uppercase }}"
})
class GreetingComponent {
    name: string;
}