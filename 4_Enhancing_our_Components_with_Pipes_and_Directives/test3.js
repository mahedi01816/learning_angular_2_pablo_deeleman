"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var myDirective = (function () {
    function myDirective(myCustomType) {
        // implementation...
    }
    return myDirective;
}());
__decorate([
    Input()
], myDirective.prototype, "otherInputPropertyName", void 0);
__decorate([
    Output()
], myDirective.prototype, "otherOutputPropertyName", void 0);
myDirective = __decorate([
    core_1.Directive({
        selector: '[selector]',
        inputs: ['inputPropertyName'],
        outputs: ['outputPropertyName'],
        host: {
            '(event1)': 'onMethod1($event)',
            '(target:event2)': 'onMethod2($event)',
            '[prop]': 'expression',
            'attributeName': 'attributeValue'
        },
        providers: [MyCustomType]
    })
], myDirective);
var TaskTooltipDirective = (function () {
    function TaskTooltipDirective() {
    }
    TaskTooltipDirective.prototype.onMouseOver = function () {
        if (!this.defaultTooltipText && this.taskTooltip) {
            this.defaultTooltipText = this.taskTooltip.innerText;
        }
        this.taskTooltip.innerText = this.task.name;
    };
    TaskTooltipDirective.prototype.onMouseOut = function () {
        if (this.taskTooltip) {
            this.taskTooltip.innerText = this.defaultTooltipText;
        }
    };
    return TaskTooltipDirective;
}());
__decorate([
    Input()
], TaskTooltipDirective.prototype, "task", void 0);
__decorate([
    Input()
], TaskTooltipDirective.prototype, "taskTooltip", void 0);
__decorate([
    HostListener('mouseover')
], TaskTooltipDirective.prototype, "onMouseOver", null);
__decorate([
    HostListener('mouseout')
], TaskTooltipDirective.prototype, "onMouseOut", null);
TaskTooltipDirective = __decorate([
    core_1.Directive({
        selector: '[task]'
    })
], TaskTooltipDirective);
