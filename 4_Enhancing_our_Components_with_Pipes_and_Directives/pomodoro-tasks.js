"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
/// Local Data Service
var TaskService = (function () {
    function TaskService() {
        this.taskStore = [];
        var tasks = [
            {
                name: "Code an HTML Table",
                deadline: "Jun 23 2015",
                pomodorosRequired: 1
            }, {
                name: "Sketch a wireframe for the new homepage",
                deadline: "Jun 24 2016",
                pomodorosRequired: 2
            }, {
                name: "Style table with Bootstrap styles",
                deadline: "Jun 25 2016",
                pomodorosRequired: 1
            }, {
                name: "Reinforce SEO with custom sitemap.xml",
                deadline: "Jun 26 2016",
                pomodorosRequired: 3
            }
        ];
        this.taskStore = tasks.map(function (task) {
            return {
                name: task.name,
                deadline: new Date(task.deadline),
                queued: false,
                pomodorosRequired: task.pomodorosRequired
            };
        });
    }
    return TaskService;
}());
/// Component classes
/// - Main Parent Component
var TaskIconsComponent = TaskIconsComponent_1 = (function () {
    function TaskIconsComponent() {
        this.icons = [];
    }
    TaskIconsComponent.prototype.ngOnInit = function () {
        this.icons.length = this.task.pomodorosRequired;
        this.icons.fill({ name: this.task.name });
    };
    return TaskIconsComponent;
}());
__decorate([
    core_1.Input()
], TaskIconsComponent.prototype, "task", void 0);
__decorate([
    core_1.Input()
], TaskIconsComponent.prototype, "size", void 0);
TaskIconsComponent = TaskIconsComponent_1 = __decorate([
    core_1.Component({
        selector: 'pomodoro-tasks',
        directives: [TaskIconsComponent_1],
        pipes: [FormattedTimePipe],
        styleUrls: ['pomodoro-tasks.css'],
        templateUrl: 'pomodoro-tasks.html'
    }),
    core_1.Component({
        selector: 'pomodoro-task-icons',
        template: "<img *ngFor=\"let icon of icons\"\n                    src=\"/assets/img/pomodoro.png\"\n                    width=\"{{size}}\">"
    })
], TaskIconsComponent);
var TasksComponent = (function () {
    function TasksComponent() {
        this.queueHeaderMapping = {
            '=0': 'No pomodoros',
            '=1': 'One pomodoro',
            'other': '# pomodoros'
        };
        var TasksService = new TasksService();
        this.tasks = taskService.taskStore;
        this.today = new Date();
        this.updateQueuedPomodoros();
    }
    TasksComponent.prototype.toggleTask = function (task) {
        task.queued = !task.queued;
        this.updateQueuedPomodoros();
    };
    TasksComponent.prototype.updateQueuedPomodoros = function () {
        this.queuedPomodoros = this.tasks
            .filter(function (task) { return task.queued; })
            .reduce(function (pomodoros, queuedTask) {
            return pomodoros + queuedTask.pomodorosRequired;
        }, 0);
    };
    return TasksComponent;
}());
;
platform_browser_dynamic_1.bootstrap(TasksComponent);
var TaskIconsComponent_1;
