"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timer_widget_component_1 = require("./timer-widget.component");
exports.TimerWidgetComponent = timer_widget_component_1.default;
var TIMER_DIRECTIVES = [
    timer_widget_component_1.default
];
exports.TIMER_DIRECTIVES = TIMER_DIRECTIVES;
