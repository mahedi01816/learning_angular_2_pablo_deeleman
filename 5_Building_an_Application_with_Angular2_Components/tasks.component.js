"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var task_icons_component_1 = require("./task-icons.component");
var task_tooltip_directive_1 = require("./task-tooltip.directive");
var shared_1 = require("../shared/shared");
var TasksComponent = (function () {
    function TasksComponent(taskService, settingsService) {
        this.taskService = taskService;
        this.settingsService = settingsService;
        this.tasks = this.taskService.taskStore;
        this.today = new Date();
        this.queueHeaderMapping = settingsService.pluralsMap.tasks;
        this.timerMinutes = settingsService.timerMinutes;
    }
    TasksComponent.prototype.ngOnInit = function () {
        this.updateQueuedPomodoros();
    };
    TasksComponent.prototype.toggleTask = function (task) {
        task.queued = !task.queued;
        this.updateQueuedPomodoros();
    };
    TasksComponent.prototype.updateQueuedPomodoros = function () {
        this.queuedPomodoros = this.tasks
            .filter(function (Task) { return Task.queued; })
            .reduce(function (pomodoros, queuedTask) {
            return pomodoros + queuedTask.pomodorosRequired;
        }, 0);
    };
    return TasksComponent;
}());
TasksComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-tasks',
        directives: [task_icons_component_1.default, task_tooltip_directive_1.default],
        pipes: [shared_1.SHARED_PIPES],
        styleUrls: ['app/tasks/tasks.component.css'],
        templateUrl: 'app/tasks/tasks.component.html'
    })
], TasksComponent);
exports.default = TasksComponent;
;
