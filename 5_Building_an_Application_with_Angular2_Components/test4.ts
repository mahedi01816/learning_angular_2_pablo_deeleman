@Component({
    selector: 'music-charts',
    directives: [MusicPlayerComponent],
    template: '<music-player></music-player>',
    providers: [
        provide(Playlist, { useFactory: () => {
            if(condition) {
                return new TopHitsPlaylist();
            } else {
                return new Playlist();
            }
        }
        })
    ]
})
class MusicChartsComponent {}