"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("angular/core");
var app_component_1 = require("./app.component");
core_1.enableProdMode();
platform_browser_dynamic_1.bootstrap(app_component_1.default, []);
