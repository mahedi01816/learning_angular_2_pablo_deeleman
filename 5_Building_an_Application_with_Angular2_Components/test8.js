"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
// The bootstrap() function returns a promise with
// a reference to the bootstrapped component
platform_browser_dynamic_1.bootstrap(app_component_1.default, []).then(function (ref) {
    platform_browser_1.enableDebugTools(ref);
});
