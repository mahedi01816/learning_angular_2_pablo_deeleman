import {Component} from '@angular/core';
import {Playlist} from './playlist';

@Component({
    selector: 'music-player',
    templateUrl: './music-player.component.html'
})
class MusicPlayerComponent {
    playlist: Playlist;

    constructor() {
        this.playlist = new Playlist();
    }
}

@Component({
    selector: 'music-player',
    templateUrl: './music-player.component.html'
})
class MusicPlayerComponent {
    playlist: Playlist;
    constructor(playlist: Playlist) {
        this.playlist = playlist;
    }
}

import { Component } from '@angular/core';
import { Playlist } from './playlist';
@Component({
    selector: 'music-player',
    templateUrl: './music-player.component.html',
    providers: [Playlist]
})
class MusicPlayerComponent {
    constructor(public playlist: Playlist) {}
}