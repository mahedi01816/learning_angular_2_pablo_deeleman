"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var timer_1 = require("./timer/timer");
var tasks_1 = require("./tasks/tasks");
var shared_1 = require("./shared/shared");
var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-app',
        directives: [timer_1.TIMER_DIRECTIVES, tasks_1.TASKS_DIRECTIVES],
        providers: [shared_1.SHARED_PROVIDERS],
        template: "\n        <nav class=\"navbar navbar-default navbar-static-top\">\n            <div class=\"container\">\n                <div class=\"navbar-header\">\n                    <strong class=\"navbar-brand\">My Pomodoro App</strong>\n                </div>\n            </div>\n        </nav>\n        <pomodoro-timer-widget></pomodoro-timer-widget>\n        <pomodoro-tasks></pomodoro-tasks>\n    "
    })
], AppComponent);
exports.default = AppComponent;
