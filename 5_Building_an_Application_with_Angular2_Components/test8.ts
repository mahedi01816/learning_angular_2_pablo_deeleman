import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableDebugTools } from '@angular/platform-browser';
import { ComponentRef } from 'angular/core';
import AppComponent from './app.component';
// The bootstrap() function returns a promise with
// a reference to the bootstrapped component
bootstrap(AppComponent, []).then((ref: ComponentRef) => {
    enableDebugTools(ref);
});