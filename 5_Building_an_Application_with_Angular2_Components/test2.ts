import { Component, Host } from '@angular/core';
@Component({
    selector: 'music-player'
})
class MusicPlayerComponent {
    constructor(@Host() playlist: Playlist) {}
}