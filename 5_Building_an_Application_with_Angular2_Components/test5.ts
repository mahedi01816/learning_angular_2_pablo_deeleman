@Component({
    selector: 'music-charts',
    directives: [MusicPlayerComponent],
    template: '<music-player></music-player>',
    providers: [
        ConditionalService,
        provide(Playlist, { useFactory: (conditionalService) => {
            if(conditionalService.isTopHits) {
                return new TopHitsPlaylist();
            } else {
                return new Playlist();
            }
        },
            deps: [ConditionalService]
        })
    ]
})
class MusicChartsComponent {}