"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TaskIconsComponent = (function () {
    function TaskIconsComponent() {
        this.icons = [];
    }
    TaskIconsComponent.prototype.ngOnInit = function () {
        this.icons.length = this.task.pomodorosRequired;
        this.icons.fill({ name: this.task.name });
    };
    return TaskIconsComponent;
}());
__decorate([
    core_1.Input()
], TaskIconsComponent.prototype, "task", void 0);
__decorate([
    core_1.Input()
], TaskIconsComponent.prototype, "size", void 0);
TaskIconsComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-task-icons',
        template: "<img *ngFor=\"let icon of icons\"\n                    src=\"/app/shared/assets/img/pomodoro.png\"\n                    width=\"{{size}}\">"
    })
], TaskIconsComponent);
exports.default = TaskIconsComponent;
