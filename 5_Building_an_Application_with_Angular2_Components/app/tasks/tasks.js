"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var task_component_1 = require("./task.component");
exports.TaskComponent = task_component_1.default;
var task_details_component_1 = require("./task-details.component");
exports.TaskDetailsComponent = task_details_component_1.default;
