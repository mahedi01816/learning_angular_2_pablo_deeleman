import TaskComponent from './task.component';
import TaskDetailsComponent from './task-details.component';

export {
    TaskComponent,
    TaskDetailsComponent
}