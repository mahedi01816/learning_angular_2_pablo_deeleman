import {Injectable} from '@angular/core';
@Injectable()
class Playlist {
    songs: string[];

    constructor(songsService: SongsService) {
        this.songs = songsService.fetch();
    }
}