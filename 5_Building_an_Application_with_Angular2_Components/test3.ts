import { Component, provide } from '@angular/core';
import { Playlist } from './playlist';
import { TopHitsPlaylist } from './top-hits-playlist';
@Component({
    selector: 'music-charts',
    directives: [MusicPlayerComponent],
    template: '<music-player></music-player>',
    providers: [provide(Playlist, { useClass: TopHitsPlaylist })]
})
class MusicChartsComponent {}