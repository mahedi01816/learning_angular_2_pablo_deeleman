import { Component, OnInit } from '@angular/core';
import { SettingsService, TaskService } from '../shared/shared';
import { RouteParams, CanReuse, OnReuse } from '@angular/router-deprecated';

routerCanReuse(): boolean {
    return true;
}
routerOnReuse(next: ComponentInstruction): void {
// No implementation yet
}