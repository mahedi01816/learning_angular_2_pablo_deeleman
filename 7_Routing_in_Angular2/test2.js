"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskEditorComponent = (function () {
    function TaskEditorComponent(title) {
        this.title = title;
    }
    TaskEditorComponent.prototype.routerOnActivate = function (next, prev) {
        this.title.setTitle('Welcome to the Task Form!');
    };
    return TaskEditorComponent;
}());
exports.default = TaskEditorComponent;
