export default class TaskEditorComponent implements OnActivate {
    constructor(private title: Title) {
    }

    routerOnActivate(next: ComponentInstruction,
                     prev: ComponentInstruction): void {
        this.title.setTitle('Welcome to the Task Form!');
    }
}