"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var timer_widget_component_1 = require("./timer-widget.component");
var TimerComponent = (function () {
    function TimerComponent() {
    }
    return TimerComponent;
}());
TimerComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-timer',
        directives: [router_deprecated_1.ROUTER_DIRECTIVES],
        template: '<router-outlet></router-outlet>'
    }),
    router_deprecated_1.RouteConfig([
        { path: '/', name: 'GenericTimer',
            component: timer_widget_component_1.default,
            useAsDefault: true }, {
            path: '/task/:id',
            name: 'TaskTimer',
            component: timer_widget_component_1.default
        }
    ])
], TimerComponent);
exports.default = TimerComponent;
