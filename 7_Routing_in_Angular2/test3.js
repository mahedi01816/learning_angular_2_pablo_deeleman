"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskEditorComponent = (function () {
    function TaskEditorComponent(title) {
        this.title = title;
    }
    TaskEditorComponent.prototype.routerOnActivate = function () {
        this.title.setTitle('Welcome to the Task Form!');
    };
    TaskEditorComponent.prototype.routerCanDeactivate = function () {
        return confirm('Are you sure you want to leave?');
    };
    TaskEditorComponent.prototype.routerOnDeactivate = function () {
        this.title.setTitle('My Angular 2 Pomodoro Timer');
    };
    return TaskEditorComponent;
}());
exports.default = TaskEditorComponent;
