import { Component } from '@angular/core';
import {
    ROUTER_DIRECTIVES,
    CanActivate,
    ComponentInstruction,
    OnActivate,
    CanDeactivate,
    OnDeactivate } from '@angular/router-deprecated';
@Component({
    selector: 'pomodoro-tasks-editor',
    directives: [ROUTER_DIRECTIVES],
    templateUrl: 'app/tasks/task-editor.component.html'
})
@CanActivate((
    next: ComponentInstruction,
    prev: ComponentInstruction): boolean => {
        let passPhrase = prompt('Say the magic words');
        return (passPhrase === 'open sesame');
    }
)
export default class TaskEditorComponent {
    }