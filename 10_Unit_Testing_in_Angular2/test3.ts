import TimerWidgetComponent from './timer-widget.component';
import { provide } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';
import { SettingsService, TaskService } from '../shared/shared';
import {
    describe,
    expect,
    it,
    inject,
    beforeEach,
    beforeEachProviders,
    setBaseTestProviders } from '@angular/core/testing';
import { TestComponentBuilder } from '@angular/compiler/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import 'rxjs/add/operator/map';
describe('timer:TimerWidgetComponent', () => {
    let testComponentBuilder: TestComponentBuilder;
    let componentFixture: any;
    // First we setup the injector with providers for our component
// dependencies and for a fixture component builder
// Note: Animation providers are not necessary
    beforeEachProviders(() => [
        TestComponentBuilder,
        SettingsService,
        TaskService,
// RouteParams is instantiated with custom values upon injecting
        provide(RouteParams, {useValue: new RouteParams({id: null})}),
// We replace the Http provider injected later in TaskService
        MockBackend,
        BaseRequestOptions,
        provide(Http, { useFactory:
            (backend:MockBackend, options:BaseRequestOptions) => {
                return new Http(backend, options);
            },
            deps: [MockBackend, BaseRequestOptions]
        }),
        TimerWidgetComponent
    ]);
// We reinstantiate the fixture component builder before each test
    beforeEach(inject([TestComponentBuilder],
        (_testComponentBuilder: TestComponentBuilder) => {
            testComponentBuilder = _testComponentBuilder;
        }
    ));
});