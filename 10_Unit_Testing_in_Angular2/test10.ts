httpLogin(credentials): Promise<boolean> {
    return new Promise(resolve => {
        const url = '/api/authentication'; // Or your own API Auth url
        const body = JSON.stringify(credentials);
        const headers = new Headers({'Content-Type':'application/json'});
        const options = new RequestOptions({ headers: headers });
        this.http.post(url, body, options)
            .map(response => response.json())
            .subscribe(authResponse => {
                    let validCredentials: boolean = false;
                    if(authResponse && authResponse.token) {
                        validCredentials = true;
                        window.sessionStorage.setItem(
                            'token',
                            authResponse.token
                        );
                    }
                    this.userIsloggedIn.emit(validCredentials);
                    resolve(validCredentials);
                },
                error => console.log(error)
            );
    });
}