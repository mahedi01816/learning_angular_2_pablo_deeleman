it('should render each image with the proper width', done => {
    testComponentBuilder.createAsync(TaskIconsComponent)
        .then(componentFixture => {
            let taskIconsComponent = componentFixture.componentInstance;
            let nativeElement = componentFixture.nativeElement;
            let actualWidth;
            taskIconsComponent.task = { pomodorosRequired: 2 };
            taskIconsComponent.size = 60;
            componentFixture.detectChanges();
            actualWidth = nativeElement
                .querySelector('img')
                .getAttribute('width');
            expect(actualWidth).toBe('60');
            done();
        })
        .catch(e => done.fail(e));
});