var _this = this;
httpLogin(credentials);
Promise < boolean > {
    return: new Promise(function (resolve) {
        var url = '/api/authentication'; // Or your own API Auth url
        var body = JSON.stringify(credentials);
        var headers = new Headers({ 'Content-Type': 'application/json' });
        var options = new RequestOptions({ headers: headers });
        _this.http.post(url, body, options)
            .map(function (response) { return response.json(); })
            .subscribe(function (authResponse) {
            var validCredentials = false;
            if (authResponse && authResponse.token) {
                validCredentials = true;
                window.sessionStorage.setItem('token', authResponse.token);
            }
            _this.userIsloggedIn.emit(validCredentials);
            resolve(validCredentials);
        }, function (error) { return console.log(error); });
    })
};
