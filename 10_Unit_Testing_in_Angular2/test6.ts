it('should initialise displaying a specific task', done => {
// We mock the TaskService provider with some fake data
    let mockTaskService = {
        taskStore: [{
            name: 'Task A'
        }, {
            name: 'Task B'
        }, {
            name: 'Task C'
        }
        ]
    };
    testComponentBuilder
        .overrideProviders(TimerWidgetComponent, [
            provide(RouteParams, { useValue: new RouteParams({ id: '1' }) }),
            provide(TaskService, { useValue: mockTaskService })
        ])
        .createAsync(TimerWidgetComponent)
        .then(componentFixture => {
            componentFixture.componentInstance.ngOnInit();
            componentFixture.detectChanges();
            expect(componentFixture.componentInstance.taskName)
                .toEqual('Task B');
            expect(componentFixture.nativeElement.querySelector('small'))
                .toHaveText('Task B');
            componentFixture.destroy();
            done();
        })
        .catch(e => done.fail(e));
});