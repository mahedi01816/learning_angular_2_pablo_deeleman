"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timer_widget_component_1 = require("./timer-widget.component");
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var shared_1 = require("../shared/shared");
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/compiler/testing");
var http_1 = require("@angular/http");
var testing_3 = require("@angular/http/testing");
require("rxjs/add/operator/map");
testing_1.describe('timer:TimerWidgetComponent', function () {
    var testComponentBuilder;
    var componentFixture;
    // First we setup the injector with providers for our component
    // dependencies and for a fixture component builder
    // Note: Animation providers are not necessary
    testing_1.beforeEachProviders(function () { return [
        testing_2.TestComponentBuilder,
        shared_1.SettingsService,
        shared_1.TaskService,
        // RouteParams is instantiated with custom values upon injecting
        core_1.provide(router_deprecated_1.RouteParams, { useValue: new router_deprecated_1.RouteParams({ id: null }) }),
        // We replace the Http provider injected later in TaskService
        testing_3.MockBackend,
        http_1.BaseRequestOptions,
        core_1.provide(http_1.Http, { useFactory: function (backend, options) {
                return new http_1.Http(backend, options);
            },
            deps: [testing_3.MockBackend, http_1.BaseRequestOptions]
        }),
        timer_widget_component_1.default
    ]; });
    // We reinstantiate the fixture component builder before each test
    testing_1.beforeEach(testing_1.inject([testing_2.TestComponentBuilder], function (_testComponentBuilder) {
        testComponentBuilder = _testComponentBuilder;
    }));
});
