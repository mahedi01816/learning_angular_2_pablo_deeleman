it('should render each image with the proper width', function (done) {
    testComponentBuilder.createAsync(TaskIconsComponent)
        .then(function (componentFixture) {
        var taskIconsComponent = componentFixture.componentInstance;
        var nativeElement = componentFixture.nativeElement;
        var actualWidth;
        taskIconsComponent.task = { pomodorosRequired: 2 };
        taskIconsComponent.size = 60;
        componentFixture.detectChanges();
        actualWidth = nativeElement
            .querySelector('img')
            .getAttribute('width');
        expect(actualWidth).toBe('60');
        done();
    })
        .catch(function (e) { return done.fail(e); });
});
