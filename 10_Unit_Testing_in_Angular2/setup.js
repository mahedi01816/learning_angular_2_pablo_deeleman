"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browserdynamic_1 = require("@angular/platform-browserdynamic");
var testing_2 = require("@angular/platform-browser/testing");
testing_1.resetBaseTestProviders();
testing_1.setBaseTestProviders(testing_2.TEST_BROWSER_STATIC_PLATFORM_PROVIDERS, [
    platform_browserdynamic_1.BROWSER_APP_DYNAMIC_PROVIDERS,
    testing_2.ADDITIONAL_TEST_BROWSER_PROVIDERS
]);
