describe('The submit component', () => {
    beforeEachProviders(() => [
        TestComponentBuilder,
        MyCustomService,
        provide(Router, { useClass: RootRouter })
    ]);
    it('should be rendered disabled by default', () => {
        expect(submitComponent.disabled).toBe(true);
    });
});