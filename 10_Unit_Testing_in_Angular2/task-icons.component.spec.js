"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var task_icons_component_1 = require("./task-icons.component");
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/compiler/testing");
testing_1.describe('tasks:TaskIconsComponent', function () {
    var testComponentBuilder;
    // First we setup the injector with providers for our component
    // and for a fixture component builder
    testing_1.beforeEachProviders(function () { return [testing_2.TestComponentBuilder]; });
    // We reinstantiate the fixture component builder
    // before each test
    testing_1.beforeEach(testing_1.inject([testing_2.TestComponentBuilder], function (_testComponentBuilder) {
        testComponentBuilder = _testComponentBuilder;
    }));
    // Specs with assertions
    testing_1.it('renders 1 image for each pomodoro session required', function (done) {
        // We create a test component fixture on runtime
        // out from the TaskIconsComponent symbol
        testComponentBuilder
            .createAsync(task_icons_component_1.default)
            .then(function (componentFixture) {
            // We fetch instances of the component and the rendered DOM
            var taskIconsComponent = componentFixture.componentInstance;
            var nativeElement = componentFixture.nativeElement;
            // We set a test value to the @Input property
            // and trigger change detection
            taskIconsComponent.task = { pomodorosRequired: 3 };
            componentFixture.detectChanges();
            // these assertions evaluate the component's surface DOM
            testing_1.expect(nativeElement.querySelectorAll('img').length).toBe(3);
            // We finally destroy the component fixture and
            // resolve the async test
            componentFixture.destroy();
            done();
        })
            .catch(function (e) { return done.fail(e); });
    });
});
