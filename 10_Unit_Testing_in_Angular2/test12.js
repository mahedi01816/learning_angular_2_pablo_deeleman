"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var task_tooltip_directive_1 = require("./task-tooltip.directive");
var testing_1 = require("@angular/core/testing");
testing_1.describe('shared:TaskTooltipDirective', function () {
    var taskTooltipDirective;
    testing_1.beforeEach(function () {
        taskTooltipDirective = new task_tooltip_directive_1.default();
    });
    testing_1.it('should update a given tooltip upon mouseover', function (done) {
        var mockTooltip = { innerText: '' };
        taskTooltipDirective.task = { name: 'Foo' };
        taskTooltipDirective.taskTooltip = mockTooltip;
        taskTooltipDirective.onMouseOver();
        testing_1.expect(mockTooltip.innerText).toBe('Foo');
        done();
    });
    testing_1.it('should restore a given tooltip upon mouseout', function (done) {
        var mockTooltip = { innerText: 'Foo' };
        taskTooltipDirective.task = { name: 'Bar' };
        taskTooltipDirective.taskTooltip = mockTooltip;
        taskTooltipDirective.onMouseOver();
        testing_1.expect(mockTooltip.innerText).toBe('Bar');
        taskTooltipDirective.onMouseOut();
        testing_1.expect(mockTooltip.innerText).toBe('Foo');
        done();
    });
});
