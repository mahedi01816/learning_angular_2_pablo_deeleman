"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_component_1 = require("./app.component");
var login_1 = require("./login/login");
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var router_deprecated_1 = require("@angular/router-deprecated");
var common_1 = require("@angular/common");
var testing_2 = require("@angular/common/testing");
var router_deprecated_2 = require("@angular/router-deprecated");
testing_1.describe('AppComponent', function () {
    var location, router;
    // We override the Router and Location providers and its own
    // dependencies in order to instantiate a fixture router to
    // trigger routing actions and a location spy
    testing_1.beforeEachProviders(function () { return [
        router_deprecated_1.RouteRegistry,
        core_1.provide(common_1.Location, { useClass: testing_2.SpyLocation }),
        core_1.provide(router_deprecated_1.Router, { useClass: router_deprecated_2.RootRouter }),
        core_1.provide(router_deprecated_1.ROUTER_PRIMARY_COMPONENT, { useValue: app_component_1.default })
    ]; });
    // We instantiate Router and Location objects before each test
    testing_1.beforeEach(testing_1.inject([router_deprecated_1.Router, common_1.Location], function (_router, _location) {
        router = _router;
        location = _location;
    }));
    // Specs with assertions
    testing_1.it('can navigate to the main tasks component', function (done) {
        // We navigate to a component and check the resulting
        // state in the URL
        router.navigate(['TasksComponent'])
            .then(function () {
            testing_1.expect(location.path()).toBe('/tasks');
            done();
        })
            .catch(function (e) { return done.fail(e); });
    });
    testing_1.it('should be able to navigate to the login component', function (done) {
        // We navigate to an URL and check the resulting state in the URL
        router.navigateByUrl('/login').then(function () {
            testing_1.expect(router.currentInstruction.component.componentType)
                .toBe(login_1.LoginComponent);
            done();
        }).catch(function (e) { return done.fail(e); });
    });
    testing_1.it('should redirect "/" requests to the tasks component', function (done) {
        // We navigate to an URL and check the resulting state in the URL
        router.navigateByUrl('/').then(function () {
            testing_1.expect(location.path()).toBe('/tasks');
            done();
        }).catch(function (e) { return done.fail(e); });
    });
});
