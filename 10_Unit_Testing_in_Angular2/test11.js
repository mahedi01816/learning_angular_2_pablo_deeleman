"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var authentication_service_1 = require("./authentication.service");
var core_1 = require("@angular/core");
var testing_1 = require("@angular/core/testing");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/http/testing");
require("rxjs/add/operator/map");
testing_1.describe('shared:AuthenticationService', function () {
    var authenticationService;
    var mockBackend;
    testing_1.beforeEachProviders(function () { return [
        testing_2.MockBackend,
        http_1.BaseRequestOptions,
        core_1.provide(http_1.Http, {
            useFactory: function (backend, options) {
                return new http_1.Http(backend, options);
            },
            deps: [testing_2.MockBackend, http_1.BaseRequestOptions]
        }),
        authentication_service_1.default
    ]; });
    testing_1.beforeEach(testing_1.inject([testing_2.MockBackend, authentication_service_1.default], function (_mockBackend, _authenticationService) {
        authenticationService = _authenticationService;
        mockBackend = _mockBackend;
    }));
    testing_1.it('can fetch a valid token when querying the Auth API', function (done) {
        var mockedResponse = new http_1.ResponseOptions({
            body: '{"token": "eyJhbGciOi"}'
        });
        mockBackend.connections.subscribe(function (connection) {
            if (connection.request.url === '/api/authentication') {
                connection.mockRespond(new http_1.Response(mockedResponse));
            }
        });
        authenticationService.httpLogin({
            username: 'foo',
            password: 'bar'
        }).then(function (success) {
            testing_1.expect(success).toBeTruthy();
            done();
        }, function (error) { return done.fail(error); });
    });
});
