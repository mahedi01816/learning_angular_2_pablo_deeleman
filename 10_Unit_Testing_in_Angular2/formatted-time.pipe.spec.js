"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formatted_time_pipe_1 = require("./formatted-time.pipe");
var testing_1 = require("@angular/core/testing");
testing_1.describe('shared:FormattedTimePipe', function () {
    var formattedTimePipe;
    testing_1.beforeEach(function () { return formattedTimePipe = new formatted_time_pipe_1.default(); });
    // Specs with assertions
    testing_1.it('should expose a transform() method', function () {
        testing_1.expect(typeof formattedTimePipe.transform).toEqual('function');
    });
    testing_1.it('should transform 50 into "0h:50m"', function () {
        testing_1.expect(formattedTimePipe.transform(50)).toEqual('0h:50m');
    });
    testing_1.it('should transform 75 into "1h:15m"', function () {
        testing_1.expect(formattedTimePipe.transform(75)).toEqual('1h:15m');
    });
    testing_1.it('should transform 100 into "1h:40m"', function () {
        testing_1.expect(formattedTimePipe.transform(100)).toEqual('1h:40m');
    });
});
