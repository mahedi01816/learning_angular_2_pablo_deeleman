it('should initialise with the pomodoro counter at 24:59', function (done) {
    // We create a test component fixture on
    // runtime out from the component symbol
    testComponentBuilder
        .createAsync(TimerWidgetComponent)
        .then(function (componentFixture) {
        // We fetch instances of the component and the rendered DOM
        var timerWidgetComponent = componentFixture.componentInstance;
        var nativeElement = componentFixture.nativeElement;
        // We execute the OnInit hook and trigger change detection
        timerWidgetComponent.ngOnInit();
        componentFixture.detectChanges();
        // These assertions evaluate the component properties
        expect(timerWidgetComponent.isPaused).toBeTruthy();
        expect(timerWidgetComponent.minutes).toEqual(24);
        expect(timerWidgetComponent.seconds).toEqual(59);
        componentFixture.destroy();
        done(); // Resolve async text
    })
        .catch(function (e) { return done.fail(e); });
});
