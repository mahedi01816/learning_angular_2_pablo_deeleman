describe('The submit component', function () {
    beforeEachProviders(function () { return [
        TestComponentBuilder,
        MyCustomService,
        provide(Router, { useClass: RootRouter })
    ]; });
    it('should be rendered disabled by default', function () {
        expect(submitComponent.disabled).toBe(true);
    });
});
