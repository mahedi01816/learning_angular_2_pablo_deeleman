it('should initialise displaying the default labels', done => {
    testComponentBuilder
        .createAsync(TimerWidgetComponent)
        .then(componentFixture => {
            componentFixture.componentInstance.ngOnInit();
            componentFixture.detectChanges();
            expect(componentFixture.componentInstance.buttonLabelKey)
                .toEqual('start');
            expect(componentFixture.nativeElement
                .querySelector('button')
                .innerHTML.trim())
                .toEqual('Start Timer');
            componentFixture.destroy();
            done();
        })
        .catch(e => done.fail(e));
});