"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var settings_service_1 = require("./settings.service");
var testing_1 = require("@angular/core/testing");
testing_1.describe('shared:SettingsService', function () {
    var settingsService;
    testing_1.beforeEach(function () {
        settingsService = new settings_service_1.default();
    });
    testing_1.it('should provide the duration for each pomodoro', function () {
        testing_1.expect(settingsService.timerMinutes).toBeDefined();
        testing_1.expect(settingsService.timerMinutes).toBeGreaterThan(0);
        testing_1.expect(settingsService.timerMinutes).not.toBeNaN();
    });
    testing_1.it('should provide plural mappings for tasks', function () {
        var tasksPluralMappings = settingsService.pluralsMap['tasks'];
        var actualProperties = Object.keys(tasksPluralMappings).sort();
        var expectedProperties = ['=0', '=1', 'other'].sort();
        testing_1.expect(tasksPluralMappings).toBeDefined();
        testing_1.expect(actualProperties).toEqual(expectedProperties);
    });
});
