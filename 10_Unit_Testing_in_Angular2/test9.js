"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var authentication_service_1 = require("./authentication.service");
var testing_1 = require("@angular/core/testing");
testing_1.describe('shared:AuthenticationService', function () {
    var authenticationService;
    testing_1.beforeEachProviders(function () { return [
        authentication_service_1.default
    ]; });
    testing_1.beforeEach(testing_1.inject([authentication_service_1.default], function (_authenticationService) {
        authenticationService = _authenticationService;
    }));
    testing_1.it('should reject invalid credentials', function (done) {
        authenticationService.login({
            username: 'foo',
            password: 'bar'
        })
            .then(function (success) {
            testing_1.expect(success).toBeFalsy();
            done();
        });
    });
    testing_1.describe('emits an event upon user auth status changes', function () {
        testing_1.it('that should be truthy for successful logins', function (done) {
            authenticationService
                .userIsloggedIn
                .subscribe(function (authStatus) {
                testing_1.expect(authStatus).toBeTruthy();
                done();
            });
            authenticationService.login({
                username: 'john.doe@mail.com',
                password: 'letmein'
            });
        });
        testing_1.it('that should be falsy for failed logins', function (done) {
            authenticationService
                .userIsloggedIn
                .subscribe(function (authStatus) {
                testing_1.expect(authStatus).toBeFalsy();
                done();
            });
            authenticationService.login({
                username: 'foo',
                password: 'bar'
            });
        });
    });
});
