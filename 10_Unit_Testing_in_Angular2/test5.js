it('should initialise displaying the default labels', function (done) {
    testComponentBuilder
        .createAsync(TimerWidgetComponent)
        .then(function (componentFixture) {
        componentFixture.componentInstance.ngOnInit();
        componentFixture.detectChanges();
        expect(componentFixture.componentInstance.buttonLabelKey)
            .toEqual('start');
        expect(componentFixture.nativeElement
            .querySelector('button')
            .innerHTML.trim())
            .toEqual('Start Timer');
        componentFixture.destroy();
        done();
    })
        .catch(function (e) { return done.fail(e); });
});
