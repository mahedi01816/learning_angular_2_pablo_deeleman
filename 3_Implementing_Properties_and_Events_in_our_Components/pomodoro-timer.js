"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var CountdownComponent = (function () {
    function CountdownComponent() {
        var _this = this;
        this.complete = new core_1.EventEmitter();
        this.progress = new core_1.EventEmitter();
        this.intervalId = setInterval(function () { return _this.tick(); }, 1000);
    }
    CountdownComponent.prototype.tick = function () {
        if (--this.seconds < 1) {
            clearTimeout(this.intervalId);
            this.complete.emit(null);
        }
        this.progress.emit(this.seconds);
    };
    return CountdownComponent;
}());
__decorate([
    core_1.Input()
], CountdownComponent.prototype, "seconds", void 0);
__decorate([
    core_1.Output()
], CountdownComponent.prototype, "complete", void 0);
__decorate([
    core_1.Output()
], CountdownComponent.prototype, "progress", void 0);
CountdownComponent = __decorate([
    core_1.Component({
        selector: 'countdown',
        template: '<h1>Time left: {{seconds}}</h1>',
        styles: ['h1 { color: #900 }'],
        encapsulation: core_1.ViewEncapsulation.Emulated
    })
], CountdownComponent);
var PomodoroTimerComponent = (function () {
    function PomodoroTimerComponent() {
    }
    // timeout: number; /* No longer required */
    PomodoroTimerComponent.prototype.onCountdownCompleted = function () {
        alert('Time up!');
    };
    return PomodoroTimerComponent;
}());
PomodoroTimerComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-timer',
        directives: [CountdownComponent],
        encapsulation: core_1.ViewEncapsulation.None,
        templateUrl: './pomodoro-timer.html'
    })
], PomodoroTimerComponent);
platform_browser_dynamic_1.bootstrap(PomodoroTimerComponent);
