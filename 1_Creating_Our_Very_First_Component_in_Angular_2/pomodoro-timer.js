"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var PomodoroTimerComponent = (function () {
    function PomodoroTimerComponent() {
        var _this = this;
        this.resetPomodoro();
        setInterval(function () { return _this.tick(); }, 1000);
    }
    PomodoroTimerComponent.prototype.resetPomodoro = function () {
        this.minutes = 24;
        this.seconds = 59;
    };
    PomodoroTimerComponent.prototype.tick = function () {
        if (--this.seconds < 0) {
            this.seconds = 59;
            if (--this.minutes < 0) {
                this.resetPomodoro();
            }
        }
    };
    return PomodoroTimerComponent;
}());
PomodoroTimerComponent = __decorate([
    core_1.Component({
        selector: 'pomodoro-timer',
        template: '<h1> {{ minutes }}:{{ seconds }} </h1>'
    })
], PomodoroTimerComponent);
platform_browser_dynamic_1.bootstrap(PomodoroTimerComponent);
