"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TimerWidgetComponent = (function () {
    function TimerWidgetComponent(settingsService, routeParams, taskService, animationBuilder, elementRef) {
        this.settingsService = settingsService;
        this.routeParams = routeParams;
        this.taskService = taskService;
        this.animationBuilder = animationBuilder;
        this.elementRef = elementRef;
        this.buttonLabelsMap = settingsService.labelsMap.timer;
        this.fadeInAnimationBuilder = animationBuilder.css();
        this.fadeInAnimationBuilder.setDuration(1000)
            .setDelay(300)
            .setFromStyles({ opacity: 0 })
            .setToStyles({ opacity: 1 });
    }
    return TimerWidgetComponent;
}());
exports.default = TimerWidgetComponent;
