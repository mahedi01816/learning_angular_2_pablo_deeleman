export default class TimerWidgetComponent implements OnInit, CanReuse, OnReuse {
    minutes: number;
    seconds: number;
    isPaused: boolean;
    buttonLabelKey: string;
    buttonLabelsMap: any;
    taskName: string;
    fadeInAnimationBuilder: CssAnimationBuilder;

    constructor(private settingsService: SettingsService,
                private routeParams: RouteParams,
                private taskService: TaskService,
                private animationBuilder: AnimationBuilder,
                private elementRef: ElementRef) {
        this.buttonLabelsMap = settingsService.labelsMap.timer;
        this.fadeInAnimationBuilder = animationBuilder.css();
        this.fadeInAnimationBuilder.setDuration(1000)
            .setDelay(300)
            .setFromStyles({opacity: 0})
            .setToStyles({opacity: 1});
    }
}