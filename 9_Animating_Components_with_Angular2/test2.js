"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animation_builder_1 = require("@angular/platformbrowser/src/animate/animation_builder");
var HighlightDirective = (function () {
    function HighlightDirective(animationBuilder, elementRef) {
        this.animationBuilder = animationBuilder;
        this.elementRef = elementRef;
        this.cssAnimationBuilder = animationBuilder.css()
            .setDuration(300)
            .setToStyles({ backgroundColor: '#fff5a0' });
    }
    HighlightDirective.prototype.colorize = function () {
        var animation = this.cssAnimationBuilder.start(this.elementRef.nativeElement);
        animation.onComplete(function () {
            animation.applyStyles({ backgroundColor: 'inherit' });
        });
    };
    return HighlightDirective;
}());
HighlightDirective = __decorate([
    core_1.Directive({
        selector: '.pomodoro-highlight',
        providers: [animation_builder_1.AnimationBuilder],
        exportAs: 'pomodoroHighlight'
    })
], HighlightDirective);
exports.default = HighlightDirective;
