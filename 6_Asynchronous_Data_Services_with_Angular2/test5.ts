var authToken = window.localStorage.getItem('auth_token');
var headers = new Headers();
headers.append('Authorization', `Token ${authToken}`);
var request = new Request({
    method: RequestMethod.Get,
    url: '/my-api/my-secured-data-store.json',
    headers: headers
});
var authRequest: Observable<Response> = http.request(request);