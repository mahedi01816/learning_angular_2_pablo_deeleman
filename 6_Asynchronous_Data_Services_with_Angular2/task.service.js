"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var TaskService = (function () {
    function TaskService(http) {
        var _this = this;
        this.http = http;
        this.taskStore = [];
        this.dataUrl = '/app/shared/data/raw-tasks.json';
        this.taskFeed = new Observable(function (observer) {
            _this.taskObserver = observer;
        });
        this.fetchTasks();
    }
    TaskService.prototype.fetchTasks = function () {
        var _this = this;
        this.http.get(this.dataUrl)
            .map(function (response) { return response.json(); })
            .map(function (stream) { return stream.map(function (res) {
            return {
                name: res.name,
                deadline: new Date(res.deadline),
                pomodorosRequired: res.pomodorosRequired,
                queued: res.queued
            };
        }); })
            .subscribe(function (tasks) {
            _this.taskStore = tasks;
            tasks.forEach(function (task) { return _this.taskObserver.next(task); });
        }, function (error) { return console.log(error); });
    };
    return TaskService;
}());
TaskService = __decorate([
    Injectable()
], TaskService);
exports.default = TaskService;
