"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Biography = (function () {
    function Biography(http) {
        var _this = this;
        http.get('/api/bio')
            .map(function (res) { return res.text(); })
            .subscribe(function (bio) { return _this.bio = bio; });
    }
    return Biography;
}());
Biography = __decorate([
    core_1.Component({
        selector: 'bio',
        providers: [http_1.Http],
        template: '<div>{{bio}}</div>'
    })
], Biography);
