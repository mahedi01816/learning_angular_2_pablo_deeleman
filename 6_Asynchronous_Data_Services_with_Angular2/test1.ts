function notifyCompletion() {
    console.log('Our asynchronous operation has been completed');
}
function asynchronousOperation(callback) {
    setTimeout(callback, 5000);
}
asynchronousOperation(notifyCompletion);