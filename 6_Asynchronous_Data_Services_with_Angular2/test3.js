function notifyCompletion() {
    console.log('Our asynchronous operation has been completed');
}
function asynchronousOperation() {
    var promise = new Promise(function (resolve, reject) {
        setInterval(resolve, 2000);
    });
    return promise;
}
asynchronousOperation().then(notifyCompletion);
