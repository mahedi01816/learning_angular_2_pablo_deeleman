import { Component } from '@angular/core';
import { Http } from '@angular/http';
@Component({
    selector: 'bio',
    providers: [Http],
    template: '<div>{{bio}}</div>'
})
class Biography {
    bio: string;
    constructor(http: Http) {
        http.get('/api/bio')
            .map((res: Response) => res.text())
            .subscribe((bio: string) => this.bio = bio);
    }
}