function notifyCompletion() {
    console.log('Our asynchronous operation has been completed');
}
function asynchronousOperation() {
    var promise = new Promise(function (resolve, reject) {
        setTimeout(resolve, 5000);
    });
    return promise;
}
asynchronousOperation().then(notifyCompletion);
