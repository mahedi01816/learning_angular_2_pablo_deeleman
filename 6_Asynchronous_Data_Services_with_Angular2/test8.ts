const body = JSON.stringify(task);
const headers = new Headers();
headers.append('Content-Type', 'application/json');
addTask(task: Task): void {
    this.http.post(this.dataUrl, body, headers)
    .map(response => response.json())
    .subscribe((task: Task) =>
        this.taskObserver.next(task);
}
);
}