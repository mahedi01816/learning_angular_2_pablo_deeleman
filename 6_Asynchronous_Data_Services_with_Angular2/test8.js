var _this = this;
var body = JSON.stringify(task);
var headers = new Headers();
headers.append('Content-Type', 'application/json');
addTask(task, Task);
void {
    this: .http.post(this.dataUrl, body, headers)
        .map(function (response) { return response.json(); })
        .subscribe(function (task) {
        return _this.taskObserver.next(task);
    })
};
;
