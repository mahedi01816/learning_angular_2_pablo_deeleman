"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var shared_1 = require("../shared");
var RouterOutletDirective = (function (_super) {
    __extends(RouterOutletDirective, _super);
    function RouterOutletDirective(_viewContainerRef, _loader, _parentRouter, nameAttr) {
        var _this = _super.call(this, _viewContainerRef, _loader, _parentRouter, nameAttr) || this;
        _this.parentRouter = _parentRouter;
        return _this;
    }
    RouterOutletDirective.prototype.activate = function (nextInstruction) {
        var requiresAuthentication = this.protectedPath === nextInstruction.urlPath;
        if (requiresAuthentication &&
            !shared_1.AuthenticationService.isAuthorized()) {
            this.parentRouter.navigateByUrl(this.loginUrl);
        }
        return _super.prototype.activate.call(this, nextInstruction);
    };
    return RouterOutletDirective;
}(router_deprecated_1.RouterOutlet));
__decorate([
    core_1.Input()
], RouterOutletDirective.prototype, "protectedPath", void 0);
__decorate([
    core_1.Input()
], RouterOutletDirective.prototype, "loginUrl", void 0);
RouterOutletDirective = __decorate([
    core_1.Directive({
        selector: 'pomodoro-router-outlet'
    }),
    __param(3, core_1.Attribute('name'))
], RouterOutletDirective);
exports.default = RouterOutletDirective;
