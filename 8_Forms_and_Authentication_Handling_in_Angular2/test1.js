"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskEditorComponent = (function () {
    function TaskEditorComponent(title, router, taskService) {
        this.title = title;
        this.router = router;
        this.taskService = taskService;
        this.task = {};
    }
    return TaskEditorComponent;
}());
exports.default = TaskEditorComponent;
