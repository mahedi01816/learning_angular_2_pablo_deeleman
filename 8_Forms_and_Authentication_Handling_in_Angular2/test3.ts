import { Component } from '@angular/core';
import { NgControlGroup } from '@angular/common';
@Component({
    selector: 'hello-form',
    template: `
        <form (submit)="sayHello(fullName)">
            <div ngControlGroup="nameControlsGroup" #fullName="ngForm">
                <div class="form-group">
                    <input type="text"
                           placeholder="First name"
                           ngControl="firstName"
                           required>
                    <input type="text"
                           placeholder="Last name"
                           ngControl="lastName"
                           required>
                </div>
            </div>
            <input type="submit" value="Say my name">
        </form>
    `
})
export default class SayHello {
    sayHello(controlGroup: NgControlGroup): void {
        if (controlGroup.control) {
            let firstName = controlGroup.control.value.firstName;
            let lastName = controlGroup.control.value.lastName;
            alert(`Hello ${firstName} ${lastName}!`);
        }
    }
}