export default class TaskEditorComponent implements OnActivate, CanDeactivate,
    OnDeactivate {
    task: Task;
    changesSaved: boolean;
    constructor(
        private title: Title,
        private router: Router,
        private taskService: TaskService) {
        this.task = <Task>{};
    }
    saveTask() {
        this.task.deadline = new Date(this.task.deadline.toString());
        this.taskService.addTask(this.task);
        this.changesSaved = true;
        this.router.navigate(['TaskList']);
    }
    routerOnActivate() {
        this.title.setTitle('Welcome to the Task Form!');
    }
    routerCanDeactivate() {
        return this.changesSaved || confirm('Are you sure you want to leave?');
    }
    routerOnDeactivate() {
        this.title.setTitle('My Angular 2 Pomodoro Timer');
    }
}