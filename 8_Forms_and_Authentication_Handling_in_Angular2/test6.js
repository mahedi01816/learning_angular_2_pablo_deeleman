"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var LoginComponent = (function () {
    function LoginComponent(formBuilder, router) {
        this.router = router;
        this.notValidCredentials = false;
        this.loginForm = formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    LoginComponent.prototype.authenticate = function () {
        var credentials = this.loginForm.value;
        this.notValidCredentials = !this.loginForm.valid &&
            this.loginForm.dirty;
        if (credentials.username === 'john.doe@mail.com' &&
            credentials.password === 'letmein') {
            this.router.navigateByUrl('/');
        }
        else {
            this.notValidCredentials = true;
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Component({
        selector: 'pomodoro-login',
        templateUrl: 'app/login/login.component.html'
    })
], LoginComponent);
exports.default = LoginComponent;
