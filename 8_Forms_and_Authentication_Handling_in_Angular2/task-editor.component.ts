export default class TaskEditorComponent implements OnActivate, CanDeactivate,
    OnDeactivate {
    taskName: string;
    constructor(private title: Title) {}
// Rest of the component remains unchanged
}