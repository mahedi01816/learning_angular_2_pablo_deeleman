"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SayHello = (function () {
    function SayHello() {
    }
    SayHello.prototype.sayHello = function (controlGroup) {
        if (controlGroup.control) {
            var firstName = controlGroup.control.value.firstName;
            var lastName = controlGroup.control.value.lastName;
            alert("Hello " + firstName + " " + lastName + "!");
        }
    };
    return SayHello;
}());
SayHello = __decorate([
    core_1.Component({
        selector: 'hello-form',
        template: "\n        <form (submit)=\"sayHello(fullName)\">\n            <div ngControlGroup=\"nameControlsGroup\" #fullName=\"ngForm\">\n                <div class=\"form-group\">\n                    <input type=\"text\"\n                           placeholder=\"First name\"\n                           ngControl=\"firstName\"\n                           required>\n                    <input type=\"text\"\n                           placeholder=\"Last name\"\n                           ngControl=\"lastName\"\n                           required>\n                </div>\n            </div>\n            <input type=\"submit\" value=\"Say my name\">\n        </form>\n    "
    })
], SayHello);
exports.default = SayHello;
