import { Component } from '@angular/core';
import {
    Control,
    ControlGroup,
    FormBuilder,
    Validators } from '@angular/common';
@Component({
    selector: 'my-login',
    providers: [FormBuilder],
    templateUrl: 'my-login.component.html'
})
export class LoginComponent {
    name: Control;
    username: Control;
    password: Control;
    signupForm: ControlGroup;
    constructor(formBuilder: FormBuilder) {
        this.name = new Control('', Validators.required);
        this.username = new Control('', Validators.required);
        this.password = new Control('', Validators.required);
        this.signupForm = formBuilder.group({
            name: this.name,
            credentials: formBuilder.group({
                username: this.username,
                password: this.password
            })
        });
    }
}