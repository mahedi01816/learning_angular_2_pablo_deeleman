login({username, password}): Promise<boolean> {
    return new Promise(resolve => {
        let validCredentials: boolean = false;
// @NOTE: In a real scenario this check
// should be performed against a web service:
        if (username === 'john.doe@mail.com' &&
            password === 'letmein') {
            validCredentials = true;
            window.sessionStorage.setItem('token', 'eyJhbGciOi');
        }
        resolve(validCredentials);
    });
}