"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskEditorComponent = (function () {
    function TaskEditorComponent(title) {
        this.title = title;
    }
    return TaskEditorComponent;
}());
exports.default = TaskEditorComponent;
