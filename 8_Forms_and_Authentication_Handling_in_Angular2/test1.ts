export default class TaskEditorComponent implements OnActivate, CanDeactivate,
    OnDeactivate {
    task: Task;
    constructor(
        private title: Title,
        private router: Router,
        private taskService: TaskService) {
        this.task = <Task>{};
    }
}