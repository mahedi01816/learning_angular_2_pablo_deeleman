"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var LoginComponent = (function () {
    function LoginComponent(formBuilder) {
        this.name = new common_1.Control('', common_1.Validators.required);
        this.username = new common_1.Control('', common_1.Validators.required);
        this.password = new common_1.Control('', common_1.Validators.required);
        this.signupForm = formBuilder.group({
            name: this.name,
            credentials: formBuilder.group({
                username: this.username,
                password: this.password
            })
        });
    }
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'my-login',
        providers: [common_1.FormBuilder],
        templateUrl: 'my-login.component.html'
    })
], LoginComponent);
exports.LoginComponent = LoginComponent;
