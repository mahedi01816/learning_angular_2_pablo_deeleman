"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskEditorComponent = (function () {
    function TaskEditorComponent(title, router, taskService) {
        this.title = title;
        this.router = router;
        this.taskService = taskService;
        this.task = {};
    }
    TaskEditorComponent.prototype.saveTask = function () {
        this.task.deadline = new Date(this.task.deadline.toString());
        this.taskService.addTask(this.task);
        this.changesSaved = true;
        this.router.navigate(['TaskList']);
    };
    TaskEditorComponent.prototype.routerOnActivate = function () {
        this.title.setTitle('Welcome to the Task Form!');
    };
    TaskEditorComponent.prototype.routerCanDeactivate = function () {
        return this.changesSaved || confirm('Are you sure you want to leave?');
    };
    TaskEditorComponent.prototype.routerOnDeactivate = function () {
        this.title.setTitle('My Angular 2 Pomodoro Timer');
    };
    return TaskEditorComponent;
}());
exports.default = TaskEditorComponent;
