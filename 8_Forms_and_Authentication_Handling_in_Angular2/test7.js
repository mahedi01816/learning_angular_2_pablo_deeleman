"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginComponent = (function () {
    function LoginComponent(formBuilder, router) {
        var _this = this;
        this.router = router;
        this.notValidCredentials = false;
        this.showUsernameHint = false;
        this.loginForm = formBuilder.group({
            username: ['', Validators.compose([
                    Validators.required,
                    this.emailValidator
                ])],
            password: ['', Validators.required]
        });
        var username = this.loginForm.controls['username'];
        username.valueChanges.subscribe(function (value) {
            _this.showUsernameHint = (username.dirty &&
                value.indexOf('@') < 0);
        });
    }
    return LoginComponent;
}());
exports.default = LoginComponent;
