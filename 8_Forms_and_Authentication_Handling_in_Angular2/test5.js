"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginComponent = (function () {
    function LoginComponent(formBuilder) {
        this.signupForm = formBuilder.group({
            name: this.formBuilder.control('', Validators.required),
            credentials: formBuilder.group({
                username: this.formBuilder.control('', Validators.required),
                password: this.formBuilder.control('', Validators.required)
            })
        });
    }
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
var LoginComponent = (function () {
    function LoginComponent(formBuilder) {
        this.signupForm = formBuilder.group({
            name: ['', Validators.required],
            credentials: formBuilder.group({
                username: ['', Validators.required],
                password: ['', Validators.required]
            })
        });
    }
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
